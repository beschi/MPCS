/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sanggam.shambk.mpcs.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *@author beschi
 */
public class LoginManager {
    DatabaseManager connection;

    public boolean login(String username, String password){
        connection = new DatabaseManager();
        try {
            String usr = "";
            String pwd = "";
            int id=0;
            String sql = "SELECT username, password, id FROM login WHERE username = '" + username + "' AND password='" + password+"'";
            ResultSet rs = connection.getRecords(sql);
            while (rs.next()) {
                usr = rs.getString(1);
                pwd = rs.getString(2);
                id = rs.getInt(3);

            }
            if (usr.equals(username) && pwd.equals(password)){
                SessionManager.currentUser = username;
                SessionManager.currentUserId = id;
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }finally{
            connection.disconnect();
        }
    }

    public int changePassword(int id, String password){
        connection = new DatabaseManager();
        int status=0;
        String sql = "UPDATE login SET password='"+password+"' WHERE id="+id;
        status= connection.updateRecord(sql);
        connection.disconnect();
        return status;
    }
    

}
