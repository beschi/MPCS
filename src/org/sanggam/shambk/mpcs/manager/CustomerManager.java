/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sanggam.shambk.mpcs.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sanggam.shambk.mpcs.bean.CustomerBean;

/**
 *
 * @author beschi
 */
public class CustomerManager {
    private DatabaseManager connection;

    public boolean checkCustomer(int regNo){
        connection = new DatabaseManager();
        boolean status = false;
        String sql = "SELECT name FROM customer WHERE regno ="+regNo;
        ResultSet resultSet = connection.getRecords(sql);
        try {
            if (resultSet.next()) {
                status = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        connection.disconnect();
        return status;
    }

    public List<Integer> getCustomerRegNos(){
        connection = new DatabaseManager();
        List<Integer> result = new ArrayList<Integer>();
        String sql ="SELECT regno FROM customer;";
        ResultSet rs = connection.getRecords(sql);
        try {
            while (rs.next()) {
                result.add(rs.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        connection.disconnect();
        return result;
    }

    public List<CustomerBean> getCustomers(){
        connection = new DatabaseManager();
        List<CustomerBean> result = new ArrayList<CustomerBean>();
        String sql ="SELECT *FROM customer ORDER BY regno;";
        ResultSet rs = connection.getRecords(sql);
        int i = 1;
        try {
            while (rs.next()) {
                CustomerBean bean = new CustomerBean();
                bean.setCustomerId(i);
                bean.setRegno(rs.getInt(1));
                bean.setName(rs.getString(2));
                bean.setGender(rs.getString(3).charAt(0));
                bean.setPlace(rs.getString(4));
                bean.setMobile(rs.getString(5));
                result.add(bean);
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        connection.disconnect();
        return result;

    }

    public int addCustomer(CustomerBean bean){
        connection = new DatabaseManager();
        String sql = "INSERT INTO customer (regno , name, gender, place, phoneno) VALUES ("+bean.getRegno()+", '"+bean.getName()+"', '"+bean.getGender()+"', '"+bean.getPlace()+"', '"+bean.getMobile()+"')";
        int status = connection.addRecord(sql);
        connection.disconnect();
        return status;
    }

    public int deleteCustomer(int id) {
        connection = new DatabaseManager();
        String sql = "DELETE FROM customer WHERE regno ="+id;
        int status = connection.deleteRecord(sql);
        connection.disconnect();
        return status;
        
    }

    public int updateCustomer(CustomerBean bean) {
       connection = new DatabaseManager();
         String sql = "UPDATE customer SET  name ='"+bean.getName()+"',gender='"+bean.getGender()+"',place='"+bean.getPlace()+"',phoneno='"+bean.getMobile()+"' WHERE regno ="+bean.getRegno();
        int status = connection.updateRecord(sql);
        connection.disconnect();
        return status;
    }

}
