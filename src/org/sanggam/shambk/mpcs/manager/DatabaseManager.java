package org.sanggam.shambk.mpcs.manager;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sanggam.shambk.mpcs.bean.DBConstants;
import org.sanggam.shambk.mpcs.resources.PropertyReader;


/**
 * @Author D Beschi Antony
 * @Date Jun 24, 2010
 * @File org.mca.mcsp060.shamkb.ontomanager.IndexManager.java
 * @Purpose
 */
public class DatabaseManager {
	private Connection connection;
	private Statement statement;
	
	
	public DatabaseManager() {
		dbConnect();
	}

	private void dbConnect() {
		try {
			//Class.forName(PropertyReader.getString("DB_DRIVER"));
                        Class.forName(DBConstants.DB_DRIVER);
			/*connection = DriverManager.getConnection(PropertyReader
					.getString("DB_URL"), PropertyReader.getString("DB_USER"),
					PropertyReader.getString("DB_PASSWD"));*/
                        connection = DriverManager.getConnection(DBConstants.DB_URL, DBConstants.DB_USER,
					DBConstants.DB_PASSWORD);

			statement = connection.createStatement();
			if (connection != null) {
				System.out.println("Connected Successfully!");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{}

	}

	public ResultSet getRecords(String sql) {
		if (connection == null) {
			dbConnect();
		}
		try {
			return statement.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public int addRecord(String sql) {
		int temp = 0;
		if (connection == null) {
			dbConnect();
		}
		try {
			statement.executeUpdate(sql);
			temp = 1;
		} catch (SQLException ex) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return temp;
	}

	public int updateRecord(String sql) {
		int temp = 0;
		if (connection == null) {
			dbConnect();
		}
		try {
			statement.executeUpdate(sql);
			temp = 1;
		} catch (SQLException ex) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return temp;
	}

	public int deleteRecord(String sql) {
		int temp = 0;
		if (connection == null) {
			dbConnect();
		}
		try {
			statement.executeUpdate(sql);
			temp = 1;
		} catch (SQLException ex) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return temp;
	}

	public void disconnect() {
		if (connection != null) {
			try {
				connection.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
	public int saveModel(InputStream model, int length, String name,
			String user) {
		java.sql.PreparedStatement ontinsert = null;
		int rows = -1;
		try {
			ontinsert = connection
					.prepareStatement("insert into ontse_model(modelName, ontsemodel,user) values(?,?,?)");
			ontinsert.setString(1, name);
			ontinsert.setBinaryStream(2, model, length);
			ontinsert.setString(3, user);
			rows = ontinsert.executeUpdate();
		} catch (Exception e) {
			System.err.println("Could not be saved" + e.getLocalizedMessage());
		}
		return rows;
	}

	public InputStream getModel(String modelName) {
		try {
			java.sql.PreparedStatement ontSelect = connection
					.prepareStatement("SELECT ontsemodel, modelName FROM ontse_model WHERE modelName =?");
			ontSelect.setString(1,modelName);
			ResultSet rset = ontSelect.executeQuery();
			if (rset.next()) {
				return rset.getBinaryStream(1);
			} else {
				return null;
			}
		} catch (Exception e) {
			System.err.println("Could not be saved" + e.getLocalizedMessage());
			return null;
		}
	}

	
}
