/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MInfo.java
 *
 * Created on Sep 27, 2010, 9:18:42 PM
 */

package org.sanggam.shambk.mpcs.ui;

import com.toedter.calendar.IDateEditor;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Color;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import org.sanggam.shambk.mpcs.bean.MilkCollectionBean;
import org.sanggam.shambk.mpcs.manager.MilkCollectionManager;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import org.sanggam.shambk.mpcs.manager.CustomerManager;


/**
 *
 * @author beschi
 */
public class MInfo extends javax.swing.JInternalFrame implements PropertyChangeListener {
    protected IDateEditor dateEditor;
    protected JCalendar jcalendar;
    protected JPopupMenu popup;
    private ChangeListener changeListener;
    private Date today;
    protected boolean dateSelected;
    private static int instants = 0;
    private MilkCollectionManager mManager;
    private DefaultTableModel model;
    private int mid;
    private boolean editMode =false;
    /** Creates new form MInfo */
    public MInfo( ) {

        Set forwardKeys = getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS);
        Set newForwardKeys = new HashSet(forwardKeys);
        newForwardKeys.add(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
        setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,newForwardKeys);

       // fillTable();
        initComponents();
        instants++;

        this.dateEditor = new JTextFieldDateEditor();

        this.dateEditor.addPropertyChangeListener("date", this);

        this.dateEditor.setDateFormatString("yyyy-MM-dd");

        this.jcalendar = new JCalendar(new Date());

        jcalendar.getDayChooser().addPropertyChangeListener("day", this);

	jcalendar.getDayChooser().setAlwaysFireDayProperty(true);

        popup = new JPopupMenu() {

            private static final long serialVersionUID = -6078272560337577761L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty("JPopupMenu.firePopupMenuCanceled");
                if (b || (!b && dateSelected) || ((isCanceled != null) && !b && isCanceled.booleanValue())) {
                    super.setVisible(b);
                }
            }
        };

        

        popup.setLightWeightPopupEnabled(true);

        popup.add(jcalendar);

        changeListener = new ChangeListener() {
			boolean hasListened = false;

			public void stateChanged(ChangeEvent e) {
				if (hasListened) {
					hasListened = false;
					return;
				}
				if (popup.isVisible()) {
                                    //&&JDateChooser.this.jcalendar.monthChooser.getComboBox().hasFocus()
					MenuElement[] me = MenuSelectionManager.defaultManager()
							.getSelectedPath();
					MenuElement[] newMe = new MenuElement[me.length + 1];
					newMe[0] = popup;
					for (int i = 0; i < me.length; i++) {
						newMe[i + 1] = me[i];
					}
					hasListened = true;
					MenuSelectionManager.defaultManager()
							.setSelectedPath(newMe);
				}
			}
		};
		MenuSelectionManager.defaultManager().addChangeListener(changeListener);

                txtDate.setEditable(false);
                txtDate.setBackground(Color.WHITE);
                txtRegNo.requestFocus();

    }

    private void fillTable(){
        mManager = new MilkCollectionManager();
        List<MilkCollectionBean> list =  mManager.getMilkData();
        Vector columns = new Vector();
        Vector rowData = new Vector();
        columns.addElement("Id");
        columns.addElement("#");
        columns.addElement("Reg No");
        columns.addElement("Name");
        columns.addElement("Quantity");
        columns.addElement("Amount");
        columns.addElement("Date");
        columns.addElement("Time");
        int sn = 1;
        for (MilkCollectionBean bean :list){
            Vector row = new Vector();
            row.add(bean.getId());
            row.add(sn);
            row.add(bean.getRegNo());
            row.add(bean.getName());
            row.add(bean.getQuantity());
            row.add(bean.getAmount());
            row.add(bean.getMdate());
            row.add(bean.getTime());
            rowData.addElement(row);
            sn++;
        }
        model  = new DefaultTableModel(rowData, columns);
        tblMInfo.setModel(model);
        tblMInfo.removeColumn(tblMInfo.getColumnModel().getColumn(0));
        tblMInfo.repaint();
        this.repaint();
        }

    public static int getInstance(){
        return instants;
    }

    public static void setInstance(int ins){
        instants = ins;
    }




    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        SimpleDateFormat simpFormat = new SimpleDateFormat("yyyy-MM-dd");
        String d = simpFormat.format(new Date());
        txtDate = new javax.swing.JTextField();
        buttonDate = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cboTime = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        txtRegNo = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        buttonCancel = new javax.swing.JButton();
        buttonExit = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtQuantity = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JTextField();
        buttonDelete = new javax.swing.JButton();
        scrollMInfoTable = new javax.swing.JScrollPane();
        tblMInfo = new javax.swing.JTable();

        setClosable(true);
        setTitle("Milk Collection Desk");
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                formComponentHidden(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        txtDate.setFocusable(false);
        txtDate.setText(d);

        buttonDate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sanggam/shambk/mpcs/resources/datechooser.gif"))); // NOI18N
        buttonDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDateActionPerformed(evt);
            }
        });
        buttonDate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                buttonDateKeyPressed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Date");

        jLabel2.setText("Time");

        cboTime.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Morning", "Evening" }));
        cboTime.setFocusable(false);

        jLabel3.setText("Reg No");

        txtRegNo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtRegNoFocusLost(evt);
            }
        });
        txtRegNo.requestFocus();

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sanggam/shambk/mpcs/resources/Knob Add.png"))); // NOI18N
        btnSave.setText("Add");
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        btnSave.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                btnSaveFocusLost(evt);
            }
        });
        btnSave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnSaveKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                btnSaveKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                btnSaveKeyTyped(evt);
            }
        });

        buttonCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sanggam/shambk/mpcs/resources/Knob Cancel.png"))); // NOI18N
        buttonCancel.setText("Cancel");
        buttonCancel.setFocusPainted(false);
        buttonCancel.setFocusable(false);
        buttonCancel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelActionPerformed(evt);
            }
        });

        buttonExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sanggam/shambk/mpcs/resources/Knob Red.png"))); // NOI18N
        buttonExit.setText("Exit");
        buttonExit.setFocusable(false);
        buttonExit.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        buttonExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonExitActionPerformed(evt);
            }
        });

        jLabel4.setText("Quantity");

        jLabel5.setText("Amount");

        buttonDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sanggam/shambk/mpcs/resources/Knob Remove Red.png"))); // NOI18N
        buttonDelete.setText("Delete");
        buttonDelete.setFocusPainted(false);
        buttonDelete.setFocusable(false);
        buttonDelete.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        buttonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(173, 173, 173)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtRegNo, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonDate, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTime, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(173, 173, 173)))
                .addGap(113, 113, 113)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(buttonExit, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                    .addComponent(buttonCancel, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                    .addComponent(buttonDelete, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonExit)
                .addContainerGap(19, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(cboTime, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(buttonDate, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtRegNo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave))
                .addGap(28, 28, 28))
        );

        scrollMInfoTable.setAutoscrolls(true);

        tblMInfo.setAutoCreateRowSorter(true);
        mManager = new MilkCollectionManager();
        List<MilkCollectionBean> list =  mManager.getMilkData();
        Vector columns = new Vector();
        Vector rowData = new Vector();
        columns.addElement("Id");
        columns.addElement("#");
        columns.addElement("Reg No");
        columns.addElement("Name");
        columns.addElement("Quantity");
        columns.addElement("Amount");
        columns.addElement("Date");
        columns.addElement("Time");
        int sn = 1;
        for (MilkCollectionBean bean :list){
            Vector row = new Vector();
            row.add(bean.getId());
            row.add(sn);
            row.add(bean.getRegNo());
            row.add(bean.getName());
            row.add(bean.getQuantity());
            row.add(bean.getAmount());
            row.add(bean.getMdate());
            row.add(bean.getTime());
            rowData.addElement(row);
            sn++;
        }
        model  = new DefaultTableModel(rowData, columns);
        tblMInfo.setModel(model);
        tblMInfo.removeColumn(tblMInfo.getColumnModel().getColumn(0));
        tblMInfo.setFocusable(false);
        tblMInfo.setPreferredSize(new java.awt.Dimension(400, 300));
        tblMInfo.setRowHeight(20);
        tblMInfo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMInfoMouseClicked(evt);
            }
        });
        scrollMInfoTable.setViewportView(tblMInfo);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(scrollMInfoTable, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 993, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollMInfoTable, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentHidden
     instants--;
    }//GEN-LAST:event_formComponentHidden

    private void buttonExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonExitActionPerformed
        this.doDefaultCloseAction();
    }//GEN-LAST:event_buttonExitActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if(editMode) {
            updateRecord();
            editMode = false;
            clearFields();
        }else {
            saveRecord();
        } 
    }//GEN-LAST:event_btnSaveActionPerformed

    private void buttonDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDateActionPerformed

                int x = buttonDate.getWidth() - (int) popup.getPreferredSize().getWidth();
                int y = buttonDate.getY() + buttonDate.getHeight();

                Calendar calendar = Calendar.getInstance();
                Date date = dateEditor.getDate();
                if (date != null) {
                    calendar.setTime(date);
                }
                jcalendar.setCalendar(calendar);
                popup.show(buttonDate, x, y);
                dateSelected = false;
    }//GEN-LAST:event_buttonDateActionPerformed

    private void txtRegNoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRegNoFocusLost
        checkExistenceOfCustomer();
    }//GEN-LAST:event_txtRegNoFocusLost

    private void btnSaveKeyReleased(java.awt.event.FocusEvent evt){
    }

    private void btnSaveFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btnSaveFocusLost
         saveRecord();
    }//GEN-LAST:event_btnSaveFocusLost

    private void tblMInfoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMInfoMouseClicked
            setValueToEdit();
    }//GEN-LAST:event_tblMInfoMouseClicked

    private void buttonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteActionPerformed
        if(editMode){
            int option = JOptionPane.showConfirmDialog(null,new String("Do you want to delete?"), "MPCS Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if(option == JOptionPane.YES_OPTION){
                deleteRecord();
            }
        }else{
            JOptionPane.showMessageDialog(null, "Please select a record to delete", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_buttonDeleteActionPerformed

    private void buttonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelActionPerformed
        clearFields();
    }//GEN-LAST:event_buttonCancelActionPerformed

    private void buttonDateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buttonDateKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_buttonDateKeyPressed

    private void btnSaveKeyReleased(java.awt.event.KeyEvent evt) {

    }

    public void btnSaveKeyTyped(java.awt.event.KeyEvent evt) {
        
    }

    private void  btnSaveKeyPressed(java.awt.event.KeyEvent evt){
        System.out.println(evt.getKeyCode());
    }
    private void saveRecord(){
        if (txtDate.getText().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please choose  date", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            txtDate.requestFocus();
        } else if (cboTime.getSelectedItem().toString().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please choose time", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            cboTime.requestFocus();
        } else if (txtRegNo.getText().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please enter register number", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            txtRegNo.requestFocus();
        } else if (txtQuantity.getText().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please enter milk quantity", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            txtQuantity.requestFocus();
        } else if (txtAmount.getText().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please enter amount", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            txtAmount.requestFocus();
        } else {
            MilkCollectionBean bean = new MilkCollectionBean();
            bean.setMdate(txtDate.getText().toString());
            bean.setTime(cboTime.getSelectedItem().toString());
            bean.setRegNo(Integer.parseInt(txtRegNo.getText().toString()));
            bean.setQuantity(Double.valueOf(txtQuantity.getText().toString()));
            bean.setAmount(Double.valueOf(txtAmount.getText().toString()));
            mManager = new MilkCollectionManager();
            if (mManager.addMilkInfo(bean)>0){
                clearFields();
                fillTable();
            } else {
            JOptionPane.showMessageDialog(null, "Error in storing record", "MPCS Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void setValueToEdit(){
        editMode = true;
        btnSave.setText("Update");
        int row = tblMInfo.getSelectedRow();
        mid = Integer.parseInt(tblMInfo.getValueAt(row, 0).toString());
        txtRegNo.setText(tblMInfo.getValueAt(row, 1).toString());
        txtQuantity.setText(tblMInfo.getValueAt(row, 3).toString());
        txtAmount.setText(tblMInfo.getValueAt(row, 4).toString());
        txtDate.setText(tblMInfo.getValueAt(row, 5).toString());
        cboTime.setSelectedItem(tblMInfo.getValueAt(row, 6));
    }

    private void deleteRecord(){
        mManager = new MilkCollectionManager();
        mManager.deleteMilkInfo(mid);
        editMode = false;
        clearFields();
        fillTable();
    }

    private void updateRecord() {
        if (txtDate.getText().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please choose  date", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            txtDate.requestFocus();
        } else if (cboTime.getSelectedItem().toString().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please choose time", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            cboTime.requestFocus();
        } else if (txtRegNo.getText().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please enter register number", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            txtRegNo.requestFocus();
        } else if (txtQuantity.getText().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please enter milk quantity", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            txtQuantity.requestFocus();
        } else if (txtAmount.getText().trim().equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null, "Please enter amount", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
            txtAmount.requestFocus();
        } else {
            MilkCollectionBean bean = new MilkCollectionBean();
            bean.setMdate(txtDate.getText().toString());
            bean.setTime(cboTime.getSelectedItem().toString());
            bean.setRegNo(Integer.parseInt(txtRegNo.getText().toString()));
            bean.setQuantity(Double.valueOf(txtQuantity.getText().toString()));
            bean.setAmount(Double.valueOf(txtAmount.getText().toString()));
            mManager = new MilkCollectionManager();
            if (mManager.updateMilkInfo(mid,bean)>0){
                clearFields();
                fillTable();
            } else {
            JOptionPane.showMessageDialog(null, "Error in updating record", "MPCS Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void clearFields(){
            txtRegNo.setText("");
            txtQuantity.setText("");
            txtAmount.setText("");
            txtRegNo.requestFocus();
    }

    private void checkExistenceOfCustomer(){
        CustomerManager cManager = new CustomerManager();
        if (!txtRegNo.getText().toString().trim().equalsIgnoreCase("")) {
           boolean status = cManager.checkCustomer(Integer.parseInt(txtRegNo.getText().toString()));
           if (!status) {
               JOptionPane.showMessageDialog(null, "Register number does not exist", "MPCS Error", JOptionPane.ERROR_MESSAGE);
               txtRegNo.setText("");
               txtRegNo.requestFocus();
           }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSave;
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonDate;
    private javax.swing.JButton buttonDelete;
    private javax.swing.JButton buttonExit;
    private javax.swing.JComboBox cboTime;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane scrollMInfoTable;
    private javax.swing.JTable tblMInfo;
    private javax.swing.JTextField txtAmount;
    private javax.swing.JTextField txtDate;
    private javax.swing.JTextField txtQuantity;
    private javax.swing.JTextField txtRegNo;
    // End of variables declaration//GEN-END:variables




    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("day")) {
			if (popup.isVisible()) {
				dateSelected = true;
				popup.setVisible(false);
				dateEditor.setDate(jcalendar.getCalendar().getTime());
                                SimpleDateFormat simpFormat = new SimpleDateFormat("yyyy-MM-dd");
                                today = dateEditor.getDate();
                                txtDate.setText( simpFormat.format(dateEditor.getDate()));
                               }
		} else if (evt.getPropertyName().equals("date")) {
			if (evt.getSource() == dateEditor) {
				firePropertyChange("date", evt.getOldValue(), evt.getNewValue());
			} else {
				dateEditor.setDate((Date) evt.getNewValue());
                                SimpleDateFormat simpFormat = new SimpleDateFormat("yyyy-MM-dd");
                                today = dateEditor.getDate();
                                txtDate.setText( simpFormat.format(dateEditor.getDate()));

			}
		}
    }
    
}
