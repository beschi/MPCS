/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PaymentRegisterGenerator.java
 *
 * Created on Jul 1, 2010, 9:37:47 AM
 */

package org.sanggam.shambk.mpcs.ui;

import com.toedter.calendar.IDateEditor;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JDesktopPane;
import javax.swing.JPanel;

import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.sanggam.shambk.mpcs.manager.CustomerManager;
import org.sanggam.shambk.mpcs.ui.report.MyiReportViewer;
import org.sanggam.shambk.mpcs.ui.util.Screen;

/**
 *
 * @author bosso
 */
public class IndividualSummaryGenerator extends javax.swing.JFrame implements PropertyChangeListener {

    protected IDateEditor dateEditor;
    protected JCalendar jcalendar;
    protected JPopupMenu popup;
    private ChangeListener changeListener;
    protected boolean dateSelected;
    protected int buttonSelected;
    private Date startDate;
    private Date endDate;
    private JDesktopPane desktopPane;
    private CustomerManager customerManager;



    /** Creates new form PaymentRegisterGenerator */
    public IndividualSummaryGenerator(JDesktopPane desktop) {
        
        initComponents();
        this.setLocation(Screen.xPosition(504), Screen.yPosition(116));
        this.desktopPane = desktop;

        this.dateEditor = new JTextFieldDateEditor();

        this.dateEditor.addPropertyChangeListener("date", this);

        this.dateEditor.setDateFormatString("yyyy-MM-dd");
        
        this.jcalendar = new JCalendar(new Date());

        jcalendar.getDayChooser().addPropertyChangeListener("day", this);

	jcalendar.getDayChooser().setAlwaysFireDayProperty(true);
       
        popup = new JPopupMenu() {

            private static final long serialVersionUID = -6078272560337577761L;

            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty("JPopupMenu.firePopupMenuCanceled");
                if (b || (!b && dateSelected) || ((isCanceled != null) && !b && isCanceled.booleanValue())) {
                    super.setVisible(b);
                }
            }
        };

        popup.setLightWeightPopupEnabled(true);

        popup.add(jcalendar);

        customerManager = new CustomerManager();
        java.util.List<Integer> rset = customerManager.getCustomerRegNos();
        for(Integer id : rset){
            comboRegNo.addItem(id);
        }


       // lastSelectedDate = date;


        startButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buttonSelected =1;
                int x = startButton.getWidth() - (int) popup.getPreferredSize().getWidth();
                int y = startButton.getY() + startButton.getHeight();

                Calendar calendar = Calendar.getInstance();
                Date date = dateEditor.getDate();
                if (date != null) {
                    calendar.setTime(date);
                }
                jcalendar.setCalendar(calendar);
                popup.show(startButton, x, y);
                dateSelected = false;

            }
        });

        endButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buttonSelected =2;
                int x = endButton.getWidth() - (int) popup.getPreferredSize().getWidth();
                int y = endButton.getY() + endButton.getHeight();

                Calendar calendar = Calendar.getInstance();
                Date date = dateEditor.getDate();
                if (date != null) {
                    calendar.setTime(date);
                }
                jcalendar.setCalendar(calendar);
                popup.show(endButton, x, y);
                dateSelected = false;

            }
        });

        changeListener = new ChangeListener() {
			boolean hasListened = false;

			public void stateChanged(ChangeEvent e) {
				if (hasListened) {
					hasListened = false;
					return;
				}
				if (popup.isVisible()) {
                                    //&&JDateChooser.this.jcalendar.monthChooser.getComboBox().hasFocus()
					MenuElement[] me = MenuSelectionManager.defaultManager()
							.getSelectedPath();
					MenuElement[] newMe = new MenuElement[me.length + 1];
					newMe[0] = popup;
					for (int i = 0; i < me.length; i++) {
						newMe[i + 1] = me[i];
					}
					hasListened = true;
					MenuSelectionManager.defaultManager()
							.setSelectedPath(newMe);
				}
			}
		};
		MenuSelectionManager.defaultManager().addChangeListener(changeListener);

                startTextField.setEditable(false);
                startTextField.setBackground(Color.WHITE);
                endTextField.setEditable(false);
                endTextField.setBackground(Color.WHITE);


    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        startLabel = new javax.swing.JLabel();
        startTextField = new javax.swing.JTextField();
        startButton = new javax.swing.JButton();
        endButton = new javax.swing.JButton();
        endTextField = new javax.swing.JTextField();
        endLabel = new javax.swing.JLabel();
        startLabel1 = new javax.swing.JLabel();
        comboRegNo = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        cancelButton = new javax.swing.JButton();
        generateButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Individual Summary Generator");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        startLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        startLabel.setText("Start Date");

        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sanggam/shambk/mpcs/resources/datechooser.gif"))); // NOI18N
        startButton.setAlignmentY(0.0F);
        startButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        startButton.setMinimumSize(new java.awt.Dimension(29, 29));

        endButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sanggam/shambk/mpcs/resources/datechooser.gif"))); // NOI18N
        endButton.setAlignmentY(0.0F);
        endButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        endButton.setMinimumSize(new java.awt.Dimension(29, 29));

        endLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        endLabel.setText("End Date");

        startLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        startLabel1.setText("Reg No");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(startLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboRegNo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(startLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(startTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(endLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(endTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(endButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(endButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(endLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                        .addComponent(endTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(startLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(startTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(startLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(comboRegNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        generateButton.setText("Generate");
        generateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(generateButton, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(generateButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(416, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.hide();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void generateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateButtonActionPerformed
            if(comboRegNo.getSelectedItem().toString().equalsIgnoreCase("")){
                JOptionPane.showMessageDialog(null, "Please Choose Register Number", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
                comboRegNo.requestFocus();
            }else if(startTextField.getText().equalsIgnoreCase("")){
                JOptionPane.showMessageDialog(null, "Please Choose Start Date", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
                startButton.requestFocus();
            }else if (endTextField.getText().equalsIgnoreCase("")){
                JOptionPane.showMessageDialog(null, "Please Choose End Date", "MPCS Warning", JOptionPane.WARNING_MESSAGE);
                endButton.requestFocus();
            }else{
                this.hide();
                HashMap<String, Object > dateMap = new HashMap<String, Object>();
                dateMap.put("registerno",Integer.parseInt(comboRegNo.getSelectedItem().toString()));
                dateMap.put("startDate", startDate);
                dateMap.put("endDate", endDate);
               
                JPanel pnlBanner = new JPanel(){
                Image backImage = Toolkit.getDefaultToolkit().getImage("images/loading-big.gif");
                @Override
                    public void paintComponent (Graphics g) {
                        g.drawImage(backImage, 0, 0, this);
                        super.paintComponent(g);
                    }
                 };
                desktopPane.add(pnlBanner);
               
                try {

                    MyiReportViewer myiReportViewer = new MyiReportViewer("reports/Individual Summary.jasper",dateMap,"Individual Summary");
                    myiReportViewer.setBounds(0,0,desktopPane.getWidth(), desktopPane.getHeight());
                    desktopPane.add(myiReportViewer);
                    
                    myiReportViewer.setVisible(true);
                    myiReportViewer.setSelected(true);
                } catch (PropertyVetoException pve) {
                    pve.printStackTrace();
                }
            }
    }//GEN-LAST:event_generateButtonActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JComboBox comboRegNo;
    private javax.swing.JButton endButton;
    private javax.swing.JLabel endLabel;
    private javax.swing.JTextField endTextField;
    private javax.swing.JButton generateButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton startButton;
    private javax.swing.JLabel startLabel;
    private javax.swing.JLabel startLabel1;
    private javax.swing.JTextField startTextField;
    // End of variables declaration//GEN-END:variables

    public void propertyChange(PropertyChangeEvent evt) {

       if (evt.getPropertyName().equals("day")) {
			if (popup.isVisible()) {
				dateSelected = true;
				popup.setVisible(false);
				dateEditor.setDate(jcalendar.getCalendar().getTime());
                                SimpleDateFormat simpFormat = new SimpleDateFormat("dd/MM/yyyy");

                                if(buttonSelected == 1){
                                    startDate = dateEditor.getDate();
                                    startTextField.setText( simpFormat.format(dateEditor.getDate()));
                                }else{
                                    endDate = dateEditor.getDate();
                                    endTextField.setText( simpFormat.format(dateEditor.getDate()));
                                }
                         }
		} else if (evt.getPropertyName().equals("date")) {
			if (evt.getSource() == dateEditor) {
				firePropertyChange("date", evt.getOldValue(), evt.getNewValue());
			} else {
				dateEditor.setDate((Date) evt.getNewValue());
                                SimpleDateFormat simpFormat = new SimpleDateFormat("dd/MM/yyyy");
                                if(buttonSelected == 1){
                                    startDate = dateEditor.getDate();
                                    startTextField.setText( simpFormat.format(dateEditor.getDate()));
                                }else{
                                    endDate = dateEditor.getDate();
                                    endTextField.setText( simpFormat.format(dateEditor.getDate()));
                               }
			}
		}
    }

}
