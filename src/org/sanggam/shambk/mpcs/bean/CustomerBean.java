/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sanggam.shambk.mpcs.bean;

/**
 *
 * @author beschi
 */
public class CustomerBean {

    private int customerId;
    private int regno;
    private String name;
    private char gender;
    private String place;
    private String mobile;

    public CustomerBean(){
    }

    public CustomerBean(int id, int reg, String name,String place, char gender, String mno) {
        this.customerId = id;
        this.regno = reg;
        this.name = name;
        this.place = place;
        this.gender = gender;
        this.mobile = mno;
    }

     public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public int getRegno() {
        return regno;
    }

    public void setRegno(int regno) {
        this.regno = regno;
    }
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
