-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.19-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema mpcs
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ mpcs;
USE mpcs;

--
-- Table structure for table `mpcs`.`customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `regno` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` char(1) NOT NULL,
  `place` varchar(50) NOT NULL,
  `phoneno` varchar(13) NOT NULL,
  PRIMARY KEY  (`regno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mpcs`.`customer`
--

/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`regno`,`name`,`gender`,`place`,`phoneno`) VALUES 
 (100,'anbu','m','v','6868'),
 (101,'santhi','f','hyderbad','797979789'),
 (103,'Amisha','f','Rajbhavan','6868687'),
 (104,'Nobel Fredrick Lourdu','m','Ayandur','1234567890');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


--
-- Table structure for table `mpcs`.`login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `username` varchar(21) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mpcs`.`login`
--

/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` (`id`,`username`,`password`) VALUES 
 (1,'admin','admin'),
 (2,'beschi','beschi');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;


--
-- Table structure for table `mpcs`.`milkcollection`
--

DROP TABLE IF EXISTS `milkcollection`;
CREATE TABLE `milkcollection` (
  `mid` int(10) unsigned zerofill NOT NULL auto_increment,
  `customer_no` int(10) unsigned NOT NULL,
  `amount` double NOT NULL,
  `quantity` double NOT NULL,
  `collection_date` date NOT NULL,
  `slot` int(11) NOT NULL,
  PRIMARY KEY  (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mpcs`.`milkcollection`
--

/*!40000 ALTER TABLE `milkcollection` DISABLE KEYS */;
INSERT INTO `milkcollection` (`mid`,`customer_no`,`amount`,`quantity`,`collection_date`,`slot`) VALUES 
 (0000000001,100,200,5,'2010-09-28',1),
 (0000000002,100,240,6,'2010-09-28',2),
 (0000000003,101,500,10,'2010-09-29',1),
 (0000000004,101,600,12,'2010-09-29',2),
 (0000000005,101,320,8,'2010-09-30',1),
 (0000000006,100,400,20,'2010-10-01',1),
 (0000000007,101,200,2,'2010-10-01',1),
 (0000000008,100,240,12,'2010-10-01',2),
 (0000000009,100,240,12,'2010-10-02',2),
 (0000000010,100,240,12,'2010-10-03',2),
 (0000000011,100,200,12,'2010-10-03',1),
 (0000000012,101,550,10,'2010-11-04',1),
 (0000000013,104,250,5,'2010-11-04',1),
 (0000000014,104,300,15,'2010-11-09',1);
/*!40000 ALTER TABLE `milkcollection` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
